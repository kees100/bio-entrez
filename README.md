# Entrez

Aquesta aplicació està disponible a [https://bio-entrez.azurewebsites.net/](https://bio-entrez.azurewebsites.net/).

La pots executar al teu ordinador amb `docker`:

```sh
docker run --rm -d --name entrez -p 80:80 registry.gitlab.com/xtec/bio-entrez
```

## Develop

Primer has de crear l'entorn de treball:

```sh
./init.sh
source ~/.bashrc
```

Executa l'aplicació:
//Perquè funcioni s'han d'obrir els dos terminals 

```sh
npm run next-dev
npm run flask-dev
```

Obro un altre terminal i amb curl localhost:5000/api/.. puc provar el flask

Per construir una imatge

```sh
./build.sh
```


#box@bio-w4:~/m14-w4$ chmod +x blast.sh (Si no em deixa executar el blast.sh //permisos )
