import os
from python_on_whales import docker

CWD = os.getcwd()


# base_dir = "blast"
# subdirs = ["blastdb", "blastdb_custom", "fasta", "queries", "results"]

output = docker.run(
        image="ncbi/blast",
        remove=True,
        volumes=[(f"{CWD}/blast/blastdb", "/blast/blastdb"), (f"{CWD}/blast/blastdb_custom", "/blast/blastdb_custom"),
                 (f"{CWD}/blast/fasta", "/blast/fasta"), (f"{CWD}/blast/queries", "/blast/queries")],
        command=["update_blastdb.pl", "pdbaa"])
        # command = ["touch", "blastdb/hello"])
print(output)
quit()
# if not os.path.exists(base_dir):
#     os.mkdir(base_dir)

# for subdir in subdirs:
#     subdir_path = os.path.join(base_dir, subdir)
#     os.mkdir(subdir_path)



def blastp(fasta_file):

    query = "/blast/queries/{}".format(fasta_file)
    
    record = __blast(["blastp", "-query", query, "-db", "pdbaa", "-outfmt", "15"])
    record = json.loads(record)
    hits = record["BlastOutput2"][0]["report"]["results"]["search"]["hits"]
    return hits[0]
    #print(json.dumps(hits[0], indent=4))




def __blast(command):
    output = docker.run(
        image="ncbi/blast",
        remove=True,
        volumes=[(f"{CWD}/blast/blastdb", "/blast/blastdb"), (f"{CWD}/blast/blastdb_custom", "/blast/blastdb_custom"),
                 (f"{CWD}/blast/fasta", "/blast/fasta"), (f"{CWD}/blast/queries", "/blast/queries")],
        command=command)
    return output






