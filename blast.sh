#!/bin/bash
npm 
if ! command -v docker &> /dev/null; then
    sudo apt update
    sudo apt install -y docker.io
fi

mkdir -p blast/blastdb blast/blastdb_custom blast/fasta blast/queries  blast/results 

docker run --rm -it -v $PWD/blast/blastdb:/blast/blastdb -v $PWD/blast/blastdb_custom:/blast/blastdb_custom -v $PWD/blast/fasta:/blast/fasta -v $PWD/blast/queries:/blast/queries -v $PWD/blast/results:/blast/results ncbi/blast /bin/bash